#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 14:32:40 2019

@author: Andreas Geiges
"""

import matplotlib.pyplot as plt
import pandas as pd
import os

emissionData = pd.read_excel('plot_data.xlsx', sheet_name = 'emission_data')
tempData = pd.read_excel('plot_data.xlsx', sheet_name = 'temperature_data')

#%%

scenarios = ['CAT Pledge High', 'NDCA0', 'NDCA10', 'NDCAFORALL10', 'NDCA33', 'NDCAFORALL33','1.5 SSP1-RCP1.9']

colorDict = {'NDCA0' : "#FC4E07",
             'NDCA33' :"#E7B800",
             'NDCAFORALL33' :"#E7B800",
             "1.5 SSP1-RCP1.9"  :"#00AFBB",
             'CAT Pledge High': 'black',
             "NDCAFORALL10": (0.2980392156862745, 0.4470588235294118, 0.6901960784313725),
             "NDCA10": (0.2980392156862745, 0.4470588235294118, 0.6901960784313725)}

#%% legendDict
legendDict = {'CAT Pledge High': 'CAT Pledge High',
              'NDCA0' :'NDC', 
              'NDCA10' :'BE10',
              'NDCAFORALL10' : 'ALL10', 
              'NDCA33': 'BE33', 
              'NDCAFORALL33' : 'ALL33',
              '1.5 SSP1-RCP1.9':'1.5 SSP1-RCP1.9'}



fig = plt.figure(num= 'Figure 1',figsize=[10,7.0], dpi=100)
plt.clf()
ax = plt.subplot2grid((2, 4), (0, 0), colspan=2)
height=.9


handles = list()
plotted_scenarios = list()

for scenario in scenarios:
        

    if 'FORALL' in scenario:
        style = '--'
    elif 'CAT Pledge High' in scenario:
        style= ':'
    else:
        style = '-'
    nonNanIdx = emissionData.columns[~emissionData.loc[scenario].isnull()]
    h = plt.plot(nonNanIdx, emissionData.loc[scenario,nonNanIdx], color = colorDict[scenario], linestyle=style)

    handles.append(h[0])
    plotted_scenarios.append(legendDict[scenario])

histEnd = 2010
plotYears = list(range(1950,histEnd+1,1))
nonNanIdx = emissionData.columns[~emissionData.loc[scenario].isnull()]   
plotYears = [x for x in nonNanIdx if (x in plotYears)]
plt.plot(plotYears, emissionData.loc[scenario,plotYears], color = 'k', linestyle='-')
#plt.xlim([1950, 2100])
plt.grid('on')    
plt.ylabel('GHG emissions\n[Gt CO2 eq / yr]', multialignment='center')


plt.xlim([1950, 2100])
plt.grid('on')    
#        plt.tight_layout()
plt.ylim([-10, 70])
plt.minorticks_on()
plt.grid(b=True, which='major', color='k', linestyle='-', linewidth=.2)
plt.grid(b=True, which='minor', color='gray', linestyle='-', linewidth=.1)

subplotDict = {
        'NDCA0': 1,
        'NDCA10' : 2,
        'NDCAFORALL10' : 2,
        'NDCA33' : 3,
        'NDCAFORALL33':3,
        '1.5 SSP1-RCP1.9':4}


for scenario in scenarios[1:]:
    
    ax = plt.subplot(2,4,4 + subplotDict[scenario])
    scenario_cat =  scenarios[0]
    nonNanIdx = tempData.columns[~tempData.loc[scenario_cat].isnull()]
    h = plt.plot(nonNanIdx, tempData.loc[scenario_cat,nonNanIdx], color = colorDict[scenario_cat], linestyle=':')

    nonNanIdx = tempData.columns[~tempData.loc[scenario].isnull()]
    if 'FORALL' in scenario:
        style = '--'
    elif 'CAT Pledge High' in scenario:
        style= ':'
    else:
        style = '-'
    h = plt.plot(nonNanIdx, tempData.loc[scenario,nonNanIdx], color = colorDict[scenario], linestyle=style)
    
    nonNanIdx = tempData.columns[~tempData.loc[scenario + '_p17'].isnull()]
    ax.fill_between(nonNanIdx,
                    tempData.loc[scenario + '_p17',nonNanIdx],
                    tempData.loc[scenario + '_p83',nonNanIdx],
                    color=colorDict[scenario],alpha=0.2,lw=0)
    
    if 'FORALL' not in scenario:
        nonNanIdx = tempData.columns[~tempData.loc['Temperature observations - HadCRUT4'].isnull()]
        h_hist = plt.plot(nonNanIdx, tempData.loc['Temperature observations - HadCRUT4',nonNanIdx], color = 'gray', linewidth=0.4)
        nonNanIdx = tempData.columns[~tempData.loc['Temperature observations - BEST'].isnull()]
        h_hist2 = plt.plot(nonNanIdx, tempData.loc['Temperature observations - BEST',nonNanIdx], color = 'gray', linewidth=0.4, linestyle='--')    
    
    
    plt.ylim(0,3.5)
    plt.xlim([1950,2100])
    plt.xticks([1950, 2000,2050, 2100],rotation=45)
    if scenario == 'NDCA0':

        plt.ylabel('GMT increase\n[°C]', multialignment='center')

    else:
        ax.set_yticklabels([])
    plt.minorticks_on()
    plt.grid(b=True, which='major', color='k', linestyle='-', linewidth=.2)
    plt.grid(b=True, which='minor', color='gray', linestyle='-', linewidth=.1)
#    sdf
handles.append(h_hist[0])
handles.append(h_hist2[0])
plotted_scenarios.append('Temp Observation - HadCRUT4')
plotted_scenarios.append('Temp Observation - BEST')
l5 = plt.legend(handles,plotted_scenarios,bbox_to_anchor=(.70,.52), loc="lower center", 
bbox_transform=fig.transFigure, ncol=1,
frameon=False)

picName = "figure1.png"
plt.savefig(picName,dpi=200)

#plt.tight_layout()
os.system ('convert -trim ' + picName + ' ' +   picName)
