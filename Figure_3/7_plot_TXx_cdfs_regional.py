# -*- coding: utf-8 -*-

import os,sys,glob,time,collections,gc,importlib
import os,sys,glob,time,collections,gc,pickle,textwrap
import numpy as np
from netCDF4 import Dataset,num2date

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import matplotlib.pyplot as plt
import matplotlib
from matplotlib import patches
from shapely import geometry

import xarray as xr

import matplotlib
from matplotlib.backends.backend_pdf import PdfPages

import cartopy.crs as ccrs
import cartopy
import seaborn as sns
import shapely
sns.set()
sns.set_style("whitegrid")

srex_reg_info={'ALA':{'color':'#006400','pos_off':(+0,+3),'summer':'JJA','winter':'DJF'},
		'WNA':{'color':'darkblue','pos_off':(+20,+15),'summer':'JJA','winter':'DJF'},
		'CNA':{'color':'#838B8B','pos_off':(+8,-4),'summer':'JJA','winter':'DJF'},
		'ENA':{'color':'#006400','pos_off':(+23,-5),'summer':'JJA','winter':'DJF'},
		'CGI':{'color':'darkcyan','pos_off':(+0,-5),'summer':'JJA','winter':'DJF'},

		'CAM':{'color':'darkcyan','pos_off':(-5,-5),'summer':'JJA','winter':'DJF'},
		'WSA':{'color':'#006400','pos_off':(0,0),'summer':'JJA','winter':'DJF'},
		'AMZ':{'color':'darkblue','pos_off':(-2,0),'summer':'JJA','winter':'DJF'},
		'NEB':{'color':'#838B8B','pos_off':(+8,-3),'summer':'JJA','winter':'DJF'},
		'SSA':{'color':'darkcyan','pos_off':(+10,-4),'summer':'JJA','winter':'DJF'},

		'SAH':{'color':'darkcyan','pos_off':(0,0),'summer':'JJA','winter':'DJF'},
		'WAF':{'color':'#838B8B','pos_off':(0,-3),'summer':'JJA','winter':'DJF'},
		'EAF':{'color':'#006400','pos_off':(0,-3),'summer':'JJA','winter':'DJF'},
		'SAF':{'color':'darkblue','pos_off':(0,0),'summer':'JJA','winter':'DJF'},

		'NEU':{'color':'#006400','pos_off':(-23,+5),'summer':'JJA','winter':'DJF'},
		'CEU':{'color':'darkblue','pos_off':(+6,+7),'summer':'JJA','winter':'DJF'},
		'CAS':{'color':'#006400','pos_off':(-6,+13),'summer':'JJA','winter':'DJF'},
		'NAS':{'color':'#838B8B','pos_off':(-6,+11),'summer':'JJA','winter':'DJF'},
		'TIB':{'color':'darkcyan','pos_off':(+5,+4),'summer':'JJA','winter':'DJF'},
		'EAS':{'color':'#006400','pos_off':(+3,0),'summer':'JJA','winter':'DJF'},
		'SAS':{'color':'darkcyan','pos_off':(0,0),'summer':'JJA','winter':'DJF'},
		'MED':{'color':'#838B8B','pos_off':(-20,+5),'summer':'JJA','winter':'DJF'},
		'WAS':{'color':'darkcyan','pos_off':(-7,-5),'summer':'JJA','winter':'DJF'},
		'SAS':{'color':'darkblue','pos_off':(0,-3),'summer':'JJA','winter':'DJF'},

		'SEA':{'color':'darkcyan','pos_off':(+5,+1),'summer':'JJA','winter':'DJF'},
		'NAU':{'color':'darkblue','pos_off':(-25,3),'summer':'JJA','winter':'DJF'},
		'SAU':{'color':'#006400','pos_off':(+4,+3),'summer':'JJA','winter':'DJF'},

		'global':{'edge':'none','color':'none','alpha':1,'pos':(-128,-28),'summer':'JJA','winter':'DJF','scaling_factor':2}}

# get this repository from here: https://gitlab.com/PeterPeter/regional_panels_on_map
sys.path.append('/Users/peterpfleiderer/Projects/git-packages/regional_panels_on_map')
import regional_panels_on_map as regional_panels_on_map; importlib.reload(regional_panels_on_map)

scenario_dict = {
	'EQW_SPLIT_NDCA02050_CATFINAL_190722_' : {'color':'#FC4E07', 'label':'NDC', 'linestyle':'-'},
	# 'EQW_SPLIT_NDCA52050_CATFINAL_190722_' : {'color':'red'},
	# 'EQW_SPLIT_NDCA102050_CATFINAL_190722_' : {'color':'orange'},
	# 'EQW_SPLIT_NDCA152050_CATFINAL_190722_' : {'color':'yellow'},
	# 'EQW_SPLIT_NDCA202050_CATFINAL_190722_' : {'color':'green'},
	# 'EQW_SPLIT_NDCAFORALL33_CATFINAL_190722_' : {'color':'#E7B800', 'label':'NDC A33 all', 'linestyle':'--'},
	'EQW_SPLIT_NDCA332050_CATFINAL_190722_' : {'color':'#E7B800', 'label':'BE33', 'linestyle':'-'},
	'SSP1_19_' : {'color':'#00AFBB', 'label':'~1.5°C', 'linestyle':'-'},
}

import seaborn as sns
sns.set_style("whitegrid")
#
from matplotlib import rc
rc('text', usetex=True)

from matplotlib import rcParams
rcParams['font.family'] = "Times New Roman"

os.chdir('/Users/peterpfleiderer/Projects/NDC_ambition/')

data = xr.open_dataset('data/TXx_pdfs_merged.nc')['pdfs']

pkl_file = open('/Users/peterpfleiderer/Projects/git-packages/regional_panels_on_map/srex_dict_py3.pkl', 'rb')
srex = pickle.load(pkl_file, encoding='latin1')	;	pkl_file.close()

polygons=srex.copy()
polygons['global']={'points':[(-180,-5),(180,-5),(180,5),(-180,5)]}

def distrs(subax,region,info_dict):
	print('________'+region+'________')
	for scenario,details in scenario_dict.items():
		scenario += period
		subax.plot(data.loc['xaxis'][0,0][::-1], np.cumsum(data.loc[scenario,'one_run_per_model',region][::-1]), color=details['color'], linestyle=details['linestyle'], zorder=50)

		cdf = np.cumsum(data.loc[scenario,'one_run_per_model',region][::-1])
		inter = cdf[np.argmin(np.abs(data.loc['xaxis'][0,0][::-1].values - 2))]
		subax.plot([10,2], [inter,inter], color=details['color'], linestyle='--', zorder = 100)
		subax.scatter([2], [inter], color=details['color'], marker='*', zorder = 100)

		if region == 'global':
			subax.text(5.7,inter+0.05,str(round(np.float(inter*100)))+'$\%$', color=details['color'], fontweight='bold')

	lb_color ='none'
	if srex_reg_info[region]['edge'] != 'none':
		lb_color = srex_reg_info[region]['edge']
	if srex_reg_info[region]['color'] != 'none':
		lb_color = srex_reg_info[region]['color']
	subax.annotate(region, xy=(0.94, 0.13), xycoords='axes fraction', color='k', weight='bold', fontsize=10, ha='right', backgroundcolor='w', zorder=1)

def axis_settings(subax,info_dict,label=False,region=None):
	# subax.set_title(period + ' ' + style)
	subax.set_xlim(6,-2)
	subax.set_ylim(0,1)
	if region == 'global':
		subax.set_ylabel('fraction of land\narea affected [$\%$]')
		subax.set_xlabel('TXx change [K]')
		subax.set_yticklabels([0,20,40,60,80,100])
		subax.yaxis.get_label().set_backgroundcolor('w')
		plt.setp(subax.get_yticklabels(), backgroundcolor="w")
		# subax.yaxis.tick_right()
	else:
		subax.set_yticklabels([])
		subax.set_xticklabels([])
	subax.locator_params(axis = 'y', nbins = 5)
	subax.locator_params(axis = 'x', nbins = 9)
	subax.grid(True,which="both",ls="--",c='gray',lw=0.35)
	subax.axvline(0, color='darkgray', zorder=0)
	subax.axvline(2, color='darkgray', linestyle='--', zorder=0)
	return(subax)

period = '2081-2100'

import regional_panels_on_map as regional_panels_on_map; importlib.reload(regional_panels_on_map)

plt.close('all')
fig,ax_map=regional_panels_on_map.regional_panels_on_map(distrs, axis_settings, polygons=polygons, reg_info=srex_reg_info, x_ext=[-180,180], y_ext=[-65,85], small_plot_size=0.08, info_dict = {1:2}, title=None)
ax_map.outline_patch.set_edgecolor('white')
with sns.axes_style("white"):
	legax = fig.add_axes([0.03,0.5,0.066,0.14], facecolor='w', zorder=4)
plt.setp(legax.spines.values(), color='k', linewidth=2)
legax.set_yticklabels([])
legax.set_xticklabels([])

legend_elements=[]

for scenario,details in scenario_dict.items():
	scenario += period
	legend_elements.append(Line2D([0], [0], color=details['color'], linestyle=details['linestyle'], label=details['label']))

legax.legend(handles=legend_elements, loc='upper right',fontsize=9,ncol=1, frameon=True, facecolor='w', framealpha=1, edgecolor='w').set_zorder(1)

# ax_leg_map=fig.add_axes([-0.05,0.05,0.35,0.2],projection=ccrs.Robinson(central_longitude=0, globe=None), zorder=4)
# regional_panels_on_map.plot_polygon_legend(ax=ax_leg_map, polygons=polygons, reg_info=srex_reg_info, y_ext=[-55,85], fontsize=4)

plt.tight_layout()
plt.savefig('plots/TXx_cdfs_SREX.png', dpi=300)
plt.savefig('plots/TXx_cdfs_SREX.pdf')
plt.close()




'''
print results
'''
region = 'global'
period = '2081-2100'
xax = data.loc['xaxis'][0,0]
for scenario,details in scenario_dict.items():
	scenario += period
	print(scenario)
	print('max:' , xax[np.argmax(data.loc[scenario,'one_run_per_model',region].values)])
	print('90pctl' , xax[np.cumsum(data.loc[scenario,'one_run_per_model',region].values) > 0.9][0])
	print('90pctl' , xax[np.cumsum(data.loc[scenario,'one_run_per_model',region].values) > 0.9][0])
	print('median' , xax[np.where(np.cumsum(data.loc[scenario,'one_run_per_model',region]) > 0.5)[0][0]])








#
