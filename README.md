# NDC_ambition_ESD_paper_material

**Geiges et al. 2020:** *Incremental improvements of 2030 targets insufficient to achieve the Paris Agreement goals*

This repository provides data as well as code for Geiges et al. 2020 data processing and figure reproduction.

Data and plot scripts used to produce the figures are located in the respective figure directories.
